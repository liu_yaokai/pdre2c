/*
MIT License

Copyright (c) 2022 Yaokai Liu

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
//
// Created by Yaokai Liu on 22-10-4.
//

#ifndef PDRE_CODEGEN_H
#define PDRE_CODEGEN_H

#include "char_t.h"
#include "objects.h"

char_t * codegen(Top * top);

char_t * codegen_seq(Seq *seq,   size_t num, size_t * code_len);
char_t * codegen_rng(Range *rng, size_t num, size_t * code_len);
char_t * codegen_set(Set *set,   size_t num, size_t * code_len);
char_t * codegen_grp(Group *grp, size_t num, size_t * code_len);
char_t * codegen_uni(Union *uni, size_t num, size_t * code_len);
char_t * codegen_var(Var * var,  size_t num, size_t * code_len);
char_t * codegen_cnt(Count *cnt, size_t num, size_t * code_len);


#endif //PDRE_CODEGEN_H
