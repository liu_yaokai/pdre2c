/*
MIT License

Copyright (c) 2022 Yaokai Liu

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
//
// Created by Yaokai Liu on 22-10-4.
//

#include "codegen.h"
#include <stdio.h>
#include "alloc.h"


#define bitsof(a) (sizeof(a) << 3)
#define len(a)      (sizeof(a) / sizeof(char_t) - 1)

#define func_type       char_s "size_t"
#define failed_case     char_s "goto __failed_match;"



char_t *get_call(void *obj, size_t num, size_t *code_len);

char_t *codegen_seq(Seq *seq, size_t num, size_t *code_len) {
    char_t sequence_fmt[] = char_s "char_t str_%x[] = char_s \""char_t_format"\";\n"
                            "for(size_t i = 0; i < %d; i ++, sp++)\n"
                            "\tif ((sp[0] == str_%x[i]) == %d))\n"
                            "\t\t"failed_case"\n";
    size_t size = (bitsof(num) / 4 + 1) + len(sequence_fmt) + seq->len;

    pdre_calloc(char_t *result, size + 1, sizeof(char_t))
    char_fun(snprintf)(
            result, size, sequence_fmt,
            num, seq->value, seq->len, num, seq->is_inverse);

    *code_len = char_fun(slen)(result);
    pdre_realloc(result, result, (*code_len + 1) * sizeof(char_t))
    result[*code_len] = char_null;

    return result;
}

char_t *codegen_rng(Range *rng, size_t num, size_t *code_len) {
    char_t range_fmt[] = char_s "if (0x%x <= ord(sp[0]) && ord(sp[0]) <= 0x%x)\n"
                         "\tsp ++;\n";
    size_t range_code_size = sizeof(range_fmt) + 2 * (bitsof(char_t) / 4 + 1);

    pdre_calloc(char_t *result, range_code_size + 1, sizeof(char_t))
    char_fun(snprintf)(
            result, range_code_size, range_fmt,
            ord(rng->start), ord(rng->end));

    *code_len = char_fun(slen)(result);
    pdre_realloc(result, result, (*code_len + 1) * sizeof(char_t))
    result[*code_len] = char_null;

    return result;
}


char_t *codegen_set(Set *set, size_t num, size_t *code_len) {
    char_t plain_fmt[] = char_s "char_t plain_%x[] = char_s \""char_t_format"\";\n"
                         "for(temp = 0; temp < 0x%x && sp[0] != plain_%x[temp]; temp ++);\n"
                         "if (temp < 0x%x)\n"
                         "\tsp ++;\n";

    size_t size = len(plain_fmt) + set->n_plains;
    size += (bitsof(set->n_plains) / 4 + 1) * 2 + (bitsof(num) / 4 + 1) * 2;

    char_t else_str[] = char_s "else ";
    size += len(else_str);
    pdre_calloc(char_t *result, (size + 1), sizeof(char_t))
    if (set->n_plains > 0) {
        char_fun(snprintf)(
                result, size, plain_fmt,
                num, set->plain, set->n_plains,
                num, set->n_plains);
        size_t c_len = char_fun(slen)(result);
        char_t *sp = result + c_len;

        char_fun(cpy)(sp, else_str);
        sp += len(else_str);
    }
    size_t c_len = char_fun(slen)(result);
    char_t *sp = result + c_len;

    size_t cur_len;
    char_t *np = NULL;
    for (size_t i = 0; i < set->n_ranges; i++) {
        np = codegen_rng(set->ranges[i], 0, code_len);
        *code_len = char_fun(slen)(np);

        cur_len = sp - result;
        pdre_realloc(result, result, (cur_len + *code_len + len(else_str) + 1) * sizeof(char_t))
        sp = result + cur_len;

        char_fun(cpy)(sp, np);
        sp += *code_len;

        char_fun(cpy)(sp, else_str);
        sp += len(else_str);

        pdre_free(np)
    }

    char_t failed_str[] = char_s "\n\t"failed_case"\n";

    cur_len = sp - result;
    pdre_realloc(result, result, (cur_len + len(failed_str) + 1) * sizeof(char_t))
    sp = result + cur_len;

    char_fun(cpy)(sp, failed_str);
    sp += len(failed_str);

    *code_len = char_fun(slen)(result);
    pdre_realloc(result, result, (*code_len + 1) * sizeof(char_t))
    result[*code_len] = char_null;

    return result;
}

char_t *header_of_grp(Group *grp, size_t num, size_t *code_len) {
    char_t grp_func_header_fmt[] = char_s "group_%x(char_t * rexp)";
    size_t size = (len(grp_func_header_fmt) + bitsof(void *) / 4 + 1);
    pdre_malloc(char_t *result, sizeof(char_t) * size)
    size_t address = (size_t) grp;
    char_fun(snprintf)(result, size, grp_func_header_fmt, address);

    *code_len = char_fun(slen)(result);
    pdre_realloc(result, result, (*code_len + 1) * sizeof(char_t))
    result[*code_len] = char_null;

    return result;
}

#define FUNC_START  char_s "{\n"
#define FUNC_END    char_s "}\n\n"

char_t *codegen_grp(Group *grp, size_t num, size_t *code_len) {
    char_t type_fmt[] = char_s "static inline size_t ";
    pdre_malloc(char_t *grp_func_header, (len(type_fmt) + 1) * sizeof(char_t))
    char_fun(cpy)(grp_func_header, type_fmt);
    char_t *grp_header = header_of_grp(grp, num, code_len);
    *code_len += len(type_fmt);
    pdre_realloc(grp_func_header, grp_func_header, sizeof(char_t) * (*code_len + 1))
    char_fun(cpy)(grp_func_header + len(type_fmt), grp_header);
    *code_len = char_fun(slen)(grp_func_header);
    pdre_free(grp_header)

    char_t grp_func_init[] = char_s "char_t * sp = rexp; size_t temp = 0;\n";
    size_t size = *code_len + len(FUNC_START) + len(grp_func_init);
    pdre_malloc(char_t *cur_body, (size + 1) * sizeof(char_t))
    char_t *sp = cur_body;

    char_fun(cpy)(sp, grp_func_header);
    sp += *code_len;
    pdre_free(grp_func_header)

    char_fun(cpy)(sp, FUNC_START);
    sp += len(FUNC_START);

    char_fun(cpy)(sp, grp_func_init);
    sp += len(grp_func_init);

    size_t cur_len;
    char_t *tp = NULL;

    for (size_t i = 0; i < grp->n_sub; i++) {
        if (grp->subObjs[i]->only_parse)
            continue;
        cur_len = sp - cur_body;
        tp = get_call(grp->subObjs[i], i, code_len);
        pdre_realloc(cur_body, cur_body, (cur_len + *code_len + 1) * sizeof(char_t))
        sp = cur_body + cur_len;
        char_fun(cpy)(sp, tp);
        sp += *code_len;
        pdre_free(tp)
    }

    char_t success_return[] = char_s "return sp - rexp;\n\n";
    cur_len = sp - cur_body;
    pdre_realloc(cur_body, cur_body, (cur_len + len(success_return) + 1) * sizeof(char_t))
    sp = cur_body + cur_len;
    char_fun(cpy)(sp, success_return);
    sp += len(success_return);

    char_t failed_frturn[] = char_s "__failed_match:\n"
                             "return 0;\n";
    cur_len = sp - cur_body;
    size_t ll = cur_len + len(FUNC_END) + len(failed_frturn);
    pdre_realloc(cur_body, cur_body, (ll + 1) * sizeof(char_t))
    sp = cur_body + cur_len;

    char_fun(cpy)(sp, failed_frturn);
    sp += len(failed_frturn);

    char_fun(cpy)(sp, FUNC_END);

    *code_len = char_fun(slen)(cur_body);
    char_t *result = cur_body;
    result[*code_len] = char_null;

    return result;
}

char_t *codegen_uni(Union *uni, size_t num, size_t *code_len) {
    Union *cur_uni = uni;
    char_t *result = get_call(cur_uni->RHS, 0, code_len);
    result = replace(result, char_s "\n\t" failed_case, char_s " ");
    size_t cur_len = char_fun(slen)(result);
    result[cur_len-- - 1] = char_null;

    char_t *lhs_code = NULL;
    if (((obj_type *) cur_uni->LHS)[0] == UNI) {
        lhs_code = codegen_uni((Union *) cur_uni->LHS, 1, code_len);
    } else {
        lhs_code = get_call(cur_uni->LHS, 0, code_len);
    }

    pdre_realloc(result, result, (cur_len + *code_len + 1) * sizeof(char_t))
    char_t *sp = result + cur_len;
    char_fun(cpy)(sp, lhs_code);

    pdre_free(lhs_code)
    *code_len = char_fun(slen)(result);
    result[*code_len] = char_null;
    return result;

}

char_t *codegen_cnt(Count *cnt, size_t num, size_t *code_len) {
    char_t repeat_fmt[] = char_s "for(size_t repeat_%x = %x; repeat_%x < %x; repeat_%x ++)";
    size_t buffer_len = len(repeat_fmt);
    pdre_malloc(char_t *cnt_fmt, (buffer_len + 1) * sizeof(char_t))
    char_fun(cpy)(cnt_fmt, repeat_fmt);

    buffer_len = char_fun(slen)(cnt_fmt);
    char_t *sp = (void *) buffer_len;
    char_t *body = get_call(cnt->obj, 0, code_len);
    buffer_len += len(FUNC_START) + *code_len + len(FUNC_END);

    pdre_realloc(cnt_fmt, cnt_fmt, (buffer_len + 1) * sizeof(char_t))
    sp = (size_t) sp + cnt_fmt;
    char_fun(cpy)(sp, FUNC_START);
    sp += len(FUNC_START);
    char_fun(cpy)(sp, body);
    sp += *code_len;
    pdre_free(body)

    char_fun(cpy)(sp, FUNC_END);
    sp += len(FUNC_END);

    pdre_calloc(char_t *result, 2 * (sp - cnt_fmt + (bitsof(size_t) / 4 + 1) + 1), sizeof(char_t))
    if (cnt->min > 0) {
        char_fun(snprintf)(
                result, buffer_len, cnt_fmt,
                num, 0, num, cnt->min, num);
    }
    *code_len = char_fun(slen)(result);
    char_t *option_fmt = replace(cnt_fmt, failed_case, char_s "break;");
    sp = result + *code_len;
    if (cnt->max == 0) {
        option_fmt = replace(option_fmt, char_s "repeat_%x < %x", char_s "");
        char_fun(snprintf)(
                sp, buffer_len, option_fmt,
                num, cnt->min, num);
    } else {
        char_fun(snprintf)(
                sp, buffer_len, option_fmt,
                num, cnt->min, num, cnt->max, num);
    }

    pdre_free(option_fmt)
    *code_len = char_fun(slen)(result);
    result[*code_len] = char_null;
    return result;
}

char_t *codegen_var(Var *var, size_t num, size_t *code_len) {
    return get_call(var->obj, num, code_len);
}


char_t *get_call(void *obj, size_t num, size_t *code_len) {
    switch (((obj_type *) obj)[0]) {
        case SEQ:
            return codegen_seq(obj, num, code_len);
        case RNG:
            return codegen_rng(obj, num, code_len);
        case SET:
            return codegen_set(obj, num, code_len);
        case GRP: {
            char_t call_fmt[] = char_s "if ((temp = "char_t_format") != 0)\n"
                                "\tsp += temp;\n"
                                "else\n"
                                "\tgoto __failed_match;\n";
            pdre_malloc(char_t *call_grp, len(call_fmt) * sizeof(char_t))
            char_t *grp_header = header_of_grp(obj, num, code_len);
            grp_header = replace(grp_header, char_s "char_t * rexp", char_s "sp");
            *code_len = len(call_fmt) + char_fun(slen)(grp_header);
            pdre_realloc(call_grp, call_grp, (*code_len + 1) * sizeof(char_t))
            char_fun(snprintf)(call_grp, *code_len, call_fmt, grp_header);

            pdre_free(grp_header)
            *code_len = char_fun(slen)(call_grp);
            return call_grp;
        }
        case UNI:
            return codegen_uni(obj, num, code_len);
        case CNT:
            return codegen_cnt(obj, num, code_len);
        case VAR:
            return codegen_var(obj, num, code_len);
        default:
            return NULL;
    }
}

char_t *codegen(Top *top) {
    char_t head_fmt[] = char_s "size_t top_%x(char_t * rexp)";
    char_t var_init_fmt[] = char_s "char_t * sp = rexp; size_t temp = 0;\n";
    size_t ll = (len(head_fmt) + (bitsof(size_t) / 4 + 1) + 100);
    pdre_malloc(char_t *code, ll * sizeof(char_t))

    char_fun(snprintf)(code, len(head_fmt) + (bitsof(size_t) / 4 + 1), head_fmt, (size_t) top);

    size_t code_len = char_fun(slen)(code);
    size_t cur_len = code_len;
    code_len += len(FUNC_START) + len(var_init_fmt);

    pdre_realloc(code, code, (code_len + 1) * sizeof(char_t))
    char_t *sp = code + cur_len;

    char_fun(cpy)(sp, FUNC_START);
    sp += len(FUNC_START);

    char_fun(cpy)(sp, var_init_fmt);
    sp += len(var_init_fmt);


    char_t include_fmt[] = char_s "#include \"char_t.h\"\n\n";
    size_t bodies_len = len(include_fmt);
    pdre_malloc(char_t *result, (bodies_len + 1) * sizeof(char_t))
    char_fun(cpy)(result, include_fmt);
    char_t *tp = NULL;

    size_t grp_count = getGrpCount();
    for (size_t i = 0; i < grp_count; i++) {
        tp = codegen_grp(getGrp(i), i, &code_len);
        pdre_realloc(result, result, (bodies_len + code_len + 1) * sizeof(char_t))
        char_t *cp = result + bodies_len;
        char_fun(cpy)(cp, tp);
        pdre_free(tp)
        bodies_len = cp - result + code_len;
    }
    bodies_len = char_fun(slen)(result);
    for (size_t i = 0; i < top->n_sub; i++) {
        if (top->subObjs[i]->only_parse)
            continue;
        cur_len = sp - code;
        tp = get_call(top->subObjs[i], i, &code_len);
        pdre_realloc(code, code, (cur_len + code_len + 1) * sizeof(char_t))
        sp = code + cur_len;
        char_fun(cpy)(sp, tp);
        sp += code_len;
        pdre_free(tp)
    }

    char_t success_return[] = char_s "return sp - rexp;\n\n";
    cur_len = sp - code;
    pdre_realloc(code, code, (cur_len + len(success_return) + 1) * sizeof(char_t))
    sp = code + cur_len;
    char_fun(cpy)(sp, success_return);
    sp += len(success_return);

    char_t failed_frturn[] = char_s "__failed_match:\n"
                             "return 0;\n";
    cur_len = sp - code;
    pdre_realloc(code, code, (cur_len + len(FUNC_END) + len(failed_frturn) + 1) * sizeof(char_t))
    sp = code + cur_len;
    char_fun(cpy)(sp, failed_frturn);
    sp += len(failed_frturn);

    char_fun(cpy)(sp, FUNC_END);

    code_len = char_fun(slen)(code);
    code_len += bodies_len;
    pdre_realloc(result, result, (code_len + 1) * sizeof(char_t))
    char_fun(cpy)(result + bodies_len, code);
    pdre_free(code)
    result[code_len] = char_null;

    return result;
}
